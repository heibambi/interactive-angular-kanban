import { NgModule } from '@angular/core';
import { RouterModule, Routes, Route } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { AuthGuard } from './user/auth.guard';

const routes: Routes = [
  { path: "", component: HomePageComponent },
  {
    path: "login",
    loadChildren: () => import("./user/user.module").then(module => module.UserModule).catch(err => console.log({
      message: "Error in loading the login module",
      err
    })),

  },
  {
    path: "kanban",
    loadChildren: () => import("./kanban/kanban.module").then(module => module.KanbanModule).catch(err => console.log({
      message: "Error in loading the kanban module",
      err,
    })),
    canActivate: [AuthGuard],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
