import { Component, Input } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { BoardService } from '../board.service';
import { Board, Task } from '../board.model';
import { MatDialog } from '@angular/material/dialog';
import { TaskDialogComponent } from '../dialogs/task-dialog.component';
import { Timestamp } from '@angular/fire/firestore';
@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent {
  @Input() board: Board;
  constructor(private boardService: BoardService, private dialog: MatDialog) { }

  taskDrop(event: CdkDragDrop<string[]>) {
    if (!this.board.tasks || !this.board.id) {
      console.log("the board cannot be inherrited from the parents component.");
      return;
    }
    moveItemInArray(this.board.tasks, event.previousIndex, event.currentIndex);
    this.boardService.updateTasks(this.board.id, this.board.tasks);
  }

  openDialog(task?: Task, index?: number) {
    const createTimeString = Timestamp.now().toDate().toDateString();
    const newTask = { label: "purple" , createdAt : createTimeString};
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '500px',
      data: task
        ? { task: { ...task }, isNew: false, boardId: this.board.id, index }
        : { task: newTask, isNew: true }
    });


    dialogRef.afterClosed().subscribe(result => {

      if (!this.board.id) {
        console.log("The board id is undefined!");
        return;
      };
      if (!this.board.tasks) {
        console.log("The board task array is undefined!");
        return;
      };

      if (!result) return;

      if (result.isNew) {
        this.boardService.updateTasks(this.board.id,
          [
            ...this.board.tasks,
            result.task
          ]);
      } else {
        const update = this.board.tasks;
        update.splice(result.index, 1, result.task);
        this.boardService.updateTasks(this.board.id, this.board.tasks)
      }

    })

  }


  handleDelete() {
    if (!this.board.id) {
      console.log("cannot delete without this board's id");
      return;
    }
    this.boardService.deleteBoard(this.board.id)
  }
}
