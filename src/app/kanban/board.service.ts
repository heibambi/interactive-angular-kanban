import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { arrayRemove } from "firebase/firestore";
import * as firebase from "firebase/compat/app";
import { switchMap, map } from 'rxjs/operators';
import { Task, Board } from "./board.model"

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  private boardAlias = "boards";
  constructor(private afAuth: AngularFireAuth, private afStore: AngularFirestore) { }

  // create new board for current user
  async createBoard(data: Board) {
    const user = await this.afAuth.currentUser;
    return this.afStore.collection(this.boardAlias).add({
      ...data,
      uid: user?.uid,
      tasks: [],
    });
  }

  // delete board 
  deleteBoard(boardID: string) {
    return this.afStore
      .collection(this.boardAlias)
      .doc(boardID)
      .delete();
  }

  // update the task array on the board
  updateTasks(boardID: string, taskArr: Task[]) {
    return this.afStore
      .collection(this.boardAlias)
      .doc(boardID)
      .update({ tasks : taskArr });
  }

  // remove specific task from the task arr on the board
  removeTask(boardID: string, task: Task) {
    return this.afStore
      .collection(this.boardAlias)
      .doc(boardID)
      .update({
        tasks: arrayRemove(task)
      })
  }
  // get all boards by current user
  getUserBoards() {
    return this.afAuth.authState.pipe(
      switchMap(user => {
        if (!user) return [];
        return this.afStore
          .collection<Board>(this.boardAlias, ref => {
            return ref.where("uid", "==", user.uid).orderBy("priority");
          })
          .valueChanges({ idField: 'id' });
      })
    );
  }

  // run a batch write to change the priority of each board for sorting
  sortBoards(boardArr: Board[]) {
    const db = firebase.default.firestore();
    const batch = db.batch();

    const refArr = boardArr.map(board => db.collection(this.boardAlias).doc(board.id));
    
    refArr.forEach((ref, index) => batch.update(ref, { priority: index }));
    batch.commit();
  }
}
