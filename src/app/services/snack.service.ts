import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SnackService {

  constructor(private snackBar:MatSnackBar , private router:Router) { }

  invalidAuth(){
    this.snackBar.open("You must be logged-in to access the following section." , "OK", {
      duration: 5000
    } );

    return this.snackBar._openedSnackBarRef?.onAction().pipe(
      tap( _ => {
        console.log({_
        ,
        message : "In the snackbar service"});
        this.router.navigate(["/login"]);
      })
    )
    .subscribe();
    
  }


}
