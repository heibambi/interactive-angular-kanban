import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { SnackService } from '../services/snack.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private afAuth: AngularFireAuth , private snackBarService:SnackService) { }

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):  Promise<boolean | UrlTree>  {
    
    const user = await this.afAuth.currentUser
    console.log({user});
    const isLoggedIn = !!user;
    if(!isLoggedIn){
      this.snackBarService.invalidAuth();
    }
    return isLoggedIn;
    
  }

  
}
