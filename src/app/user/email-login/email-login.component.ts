import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FirebaseError } from '@firebase/util'
type loginType = "login" | "signup" | "reset";

@Component({
  selector: 'app-email-login',
  templateUrl: './email-login.component.html',
  styleUrls: ['./email-login.component.scss']
})
export class EmailLoginComponent implements OnInit {

  form:FormGroup;
  loginType: loginType;
  isLoadingState : boolean;
  serverMessage: string ;

  constructor(private afAuth : AngularFireAuth , private fb : FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email : ["", [Validators.required, Validators.email]],
      password : ["", [Validators.required, Validators.minLength(6)]],
      passwordConfirm : ["", []],
    });
  }

  public changeLoginType (targetType : loginType) {
    this.loginType = targetType;
  }

  get isLogin(){
    return this.loginType === "login";
  }
  get isSignup(){
    return this.loginType === "signup";
  }
  get isReset(){
    return this.loginType === "reset";
  }

  get emailState(){
    return this.form.get("email");
  }
  get passwordState(){
    return this.form.get("password");
  }
  get passwordConfirmState(){
    return this.form.get("passwordConfirm");
  }

  get passwordDoesMatch():boolean{
    if (this.loginType !== "signup"){
      return true;
    } 

    if (this.passwordState?.value === this.passwordConfirmState?.value){
      return true;
    }

    return false;
  }

  async onSubmit(){
    this.isLoadingState = true;

    const emailValue = this.emailState?.value;
    const passwordValue = this.passwordState?.value;

    try{

      if(this.isLogin){
        await this.afAuth.signInWithEmailAndPassword(emailValue, passwordValue);
      }

      if(this.isSignup){
        await this.afAuth.createUserWithEmailAndPassword(emailValue, passwordValue);
      }

      if(this.isReset){
        await this.afAuth.sendPasswordResetEmail(emailValue);
        this.serverMessage = "Message sent! Check your email!"
      }

    } catch (error : (unknown | FirebaseError)){
      if (error instanceof FirebaseError){
        this.serverMessage = error.message;
      } else {
        console.log({error})
      }
    }

    this.isLoadingState = false;
  }

}
