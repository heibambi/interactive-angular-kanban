import { Directive, Input, AfterContentInit, ElementRef } from '@angular/core';

@Directive({
  selector: '[autoFocusDirective]'
})
export class AutofocusDirective implements AfterContentInit{
  @Input() public appAutoFocus: boolean;
  constructor(private el:ElementRef) { }
  
  public ngAfterContentInit(){
    setTimeout(() => {
      console.log("autoFocusFired");
      this.el.nativeElement.focus();
    }, 100);
  }
}
