// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyDDT76yfLYSN7VVXTQWC7uJLVLk7KWjkEc",
    authDomain: "ngkanban-da180.firebaseapp.com",
    projectId: "ngkanban-da180",
    storageBucket: "ngkanban-da180.appspot.com",
    messagingSenderId: "338970586318",
    appId: "1:338970586318:web:0c7740ae193921755ff49f",
    measurementId: "G-ZBBS95K82N"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
